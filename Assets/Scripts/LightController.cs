﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LightController : MonoBehaviour 
{
    public float currentIntensity;
    float dumper;

    [SerializeField] Light lamp;
    [SerializeField] TextMesh lightIntensityTxt;
    
    string prefix;

    private void Awake()
    {
        prefix = "L";
        lamp = GetComponentInChildren<Light>();
        lightIntensityTxt = GetComponentInChildren<TextMesh>();
    }

    public void SetLightIntensity(float intensity)
    {
        currentIntensity = intensity;
    }

    private void Update()
    {
        dumper  = Mathf.Lerp(dumper, currentIntensity, 8f * Time.deltaTime);
        lamp.intensity = dumper;
        lightIntensityTxt.text = string.Concat(prefix, dumper.ToString("0.0"));
    }
}

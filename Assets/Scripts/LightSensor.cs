﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSensor : MonoBehaviour 
{
    public static float currentEnvLightIntensity;

    WaitForSeconds waitForSeconds;

    private void Awake()
    {
        waitForSeconds = new WaitForSeconds(0.2f);
    }

    void Start () 
	{
		StartCoroutine(GetLightIntensity());
	}

    IEnumerator GetLightIntensity()
    {
        while (true)
        {
            currentEnvLightIntensity = RenderSettings.ambientIntensity;
            yield return waitForSeconds;
        }
    }
}

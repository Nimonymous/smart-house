﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurtainsConrtoller : MonoBehaviour
{
    [SerializeField] Transform curtainsRoot;

    [SerializeField] bool currentState;

    private void Awake()
    {
        curtainsRoot = transform.GetChild(0);
    }

    public void SwitchCurtainsState(bool isOpened)
    {
        currentState = isOpened;
    }

    private void Update()
    {
        if (currentState)
        {
            Vector3 localScale = curtainsRoot.localScale;
            localScale.y = Mathf.Lerp(localScale.y, 0, 1f * Time.deltaTime);
            curtainsRoot.localScale = localScale;
        }
        else
        {
            Vector3 localScale = curtainsRoot.localScale;
            localScale.y = Mathf.Lerp(localScale.y, 1, 1f * Time.deltaTime);
            curtainsRoot.localScale = localScale;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.Net.Sockets;
using UnityEngine.UI;
using System.Threading;
using System.IO;
using System.Linq;
using System.Security.Cryptography;

public class ServerInputManager : MonoBehaviour 
{
    public static ServerInputManager inst;

    public static RSACryptoServiceProvider rsaCryptoServiceProvider;
    public static AesCryptoServiceProvider aesCryptoServiceProvider;
    public static AESKeyWrapper aesKeyWrapper;

    public static string password;

    public Text usersTable;
    [SerializeField] InputField portInput;
    [SerializeField] Button startButton;

    [SerializeField] Text localIPAddress;

    TcpListener tcpListener;
    List<ClientHandler> clients;

    BinaryReader binaryReader;
    BinaryWriter binaryWriter;

    int port;

    private void Awake()
    {
        inst = this;
        clients = new List<ClientHandler>();

        rsaCryptoServiceProvider = new RSACryptoServiceProvider();
        aesCryptoServiceProvider = new AesCryptoServiceProvider();
    }

    private void Start()
    {
        IPHostEntry hostEntry = Dns.GetHostEntry(Dns.GetHostName());
        string localAddress = hostEntry.AddressList.Single(x => x.AddressFamily == AddressFamily.InterNetwork).ToString();
        localIPAddress.text = localAddress;

        ReadXMLKey();
    }

    public void ReadXMLKey()
    {
        rsaCryptoServiceProvider.FromXmlString(File.ReadAllText(Application.streamingAssetsPath + "/XMLKey.txt"));
    }

    public void GenerateXMLKey()
    {
        File.WriteAllText(Application.streamingAssetsPath + "/XMLKey.txt", rsaCryptoServiceProvider.ToXmlString(true));
    }

    public void SetPort(InputField inputField)
    {
        if (inputField.text.Length > 1)
            port = System.Convert.ToInt32(inputField.text);
    }

    public void SetPassword(InputField inputField)
    {
        password = inputField.text;
    }

    public void StartServer() 
    {
        byte[] key = rsaCryptoServiceProvider.Encrypt(aesCryptoServiceProvider.Key, false);
        byte[] iv = rsaCryptoServiceProvider.Encrypt(aesCryptoServiceProvider.IV, false);

        aesKeyWrapper = new AESKeyWrapper();
        aesKeyWrapper.aesKey = key;
        aesKeyWrapper.aesIV = iv;

        DoListening();
    }

    async void DoListening()
    {
        tcpListener = new TcpListener(IPAddress.Any, port);
        tcpListener.Start();
        print("Started... Port: " + port + " | Password: " + password);
        startButton.interactable = false;
        portInput.interactable = false;

        while (true)
        {
            var tcpClient = await tcpListener.AcceptTcpClientAsync();
            binaryReader = new BinaryReader(tcpClient.GetStream());
            usersTable.text += tcpClient.Client.RemoteEndPoint.ToString() + "\n";
            clients.Add(new ClientHandler(tcpClient, tcpClient.Client.RemoteEndPoint.ToString()));
        }
    }
}

public class ClientHandler
{
    public string userName;

    TcpClient tcpClient;
    Thread thread;

    BinaryReader reader;
    BinaryWriter writer;

    NetworkStream stream;

    public ClientHandler(TcpClient tcpClient, string username)
    {
        this.tcpClient = tcpClient;
        stream = tcpClient.GetStream();

        reader = new BinaryReader(stream);
        writer = new BinaryWriter(stream);

        thread = new Thread(StartReading);
        thread.Start();
    }

    public void WriteToClient(string message)
    {
        writer.Write(Encrypt(message));
        writer.Flush();
    }

    void StartReading()
    {
        DataWrapper dataWrapper = null;

        writer.Write(JsonUtility.ToJson(ServerInputManager.aesKeyWrapper));
        writer.Flush();

        string password;

        while ((password = Decrypt(reader.ReadString())) != ServerInputManager.password)
        {
            writer.Write(Encrypt("false"));
            writer.Flush();
        }

        writer.Write(Encrypt("true"));
        writer.Flush();

        while (true)
        {
            string data = Decrypt(reader.ReadString());
            dataWrapper = JsonUtility.FromJson<DataWrapper>(data);
            MainManager.CallMethod(dataWrapper.methodName, dataWrapper.methodParam);
        }

    }

    public string Encrypt(string str)
    {
        ICryptoTransform cryptoTransform = ServerInputManager.aesCryptoServiceProvider.CreateEncryptor();

        using (MemoryStream ms = new MemoryStream())
        {
            using (CryptoStream cs = new CryptoStream(ms, cryptoTransform, CryptoStreamMode.Write))
            using (StreamWriter sw = new StreamWriter(cs))
                sw.WriteLine(str);
   
            return JsonUtility.ToJson(new BytesWrapper() { bytes = ms.ToArray() });
        }
    }

    public string Decrypt(string encStr)
    {
        ICryptoTransform cryptoTransform = ServerInputManager.aesCryptoServiceProvider.CreateDecryptor();

        using (MemoryStream ms = new MemoryStream(JsonUtility.FromJson<BytesWrapper>(encStr).bytes))
        {
            using (CryptoStream cs = new CryptoStream(ms, cryptoTransform, CryptoStreamMode.Read))
            using (StreamReader sr = new StreamReader(cs))
                return sr.ReadLine();
        }
    }
}

[System.Serializable]
public class DataWrapper
{
    public string methodName;
    public MethodParam methodParam;
}

[System.Serializable]
public class AESKeyWrapper
{
    public byte[] aesKey;
    public byte[] aesIV;
}

[System.Serializable]
public class BytesWrapper
{
    public byte[] bytes;
}
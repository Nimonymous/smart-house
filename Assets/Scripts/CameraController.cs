﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraController : MonoBehaviour 
{
    [SerializeField] float rotationSpeed;
    [SerializeField] float movementSpeed;

    [SerializeField] Transform freeCamPoint;
    [SerializeField] Transform topViewPoint;
    [SerializeField] Transform dummy;
    [SerializeField] Transform heightPoint;
    Quaternion targetRotation;
    Camera mainCamera;

    bool isTopView;
    bool isBlocked;

    void Awake()
    {
        mainCamera = Camera.main;
    }
	
	void Update () 
	{
        if (isTopView || isBlocked)
            return;

        if(Input.GetMouseButtonDown(1))
            Cursor.visible = false;
        if (Input.GetMouseButtonUp(1))
            Cursor.visible = true;

        if (Input.GetMouseButton(1))
        {
            Vector3 mousePosition = Input.mousePosition;
            mousePosition.z = 100f;
            mousePosition = mainCamera.ScreenToWorldPoint(mousePosition);
            Vector3 direction = (mousePosition - transform.position).normalized;
            targetRotation = Quaternion.LookRotation(direction);

            Vector3 currentEulers = targetRotation.eulerAngles;
            currentEulers.x = Mathf.Clamp(currentEulers.x, 20f, 70f);
            targetRotation.eulerAngles = currentEulers;

            direction.y = 0f;
            dummy.forward = direction;
        }

        if (Input.GetKey(KeyCode.W))
            transform.Translate(dummy.forward * movementSpeed * Time.deltaTime, Space.World);
        if (Input.GetKey(KeyCode.S))
            transform.Translate(-dummy.forward * movementSpeed * Time.deltaTime, Space.World);
        if (Input.GetKey(KeyCode.D))
            transform.Translate(dummy.right * movementSpeed * Time.deltaTime, Space.World);
        if (Input.GetKey(KeyCode.A))
            transform.Translate(-dummy.right * movementSpeed * Time.deltaTime, Space.World);  

        Vector3 currentPos = transform.position;
        currentPos.x = Mathf.Clamp(currentPos.x, -2f, 2f);
        currentPos.z = Mathf.Clamp(currentPos.z, -2f, 2f);
        transform.position = currentPos;

        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
	}

    public void SwitchView(int state)
    {
        isTopView = state == 0 ? true : false;
        targetRotation = Quaternion.Euler(55f, -90f, 0f);

        StopAllCoroutines();

        if (isTopView)
            StartCoroutine(LerpPosition(topViewPoint));
        else
            StartCoroutine(LerpPosition(freeCamPoint));
    }

    IEnumerator LerpPosition(Transform targetPoint)
    {
        isBlocked = true;
        while (Vector3.Distance(targetPoint.position, transform.position) > 0.001f)
        {
            transform.position = Vector3.Lerp(transform.position, targetPoint.position, 8f * Time.deltaTime);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetPoint.rotation, 8f * Time.deltaTime);
            yield return null;
        }

        isBlocked = false;
    }
}

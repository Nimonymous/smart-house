﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Room : MonoBehaviour 
{
    public enum LightMode
    {
        Manual,
        Sensor,
        History
    }

    public enum TemperatureMode
    {
        Manual,
        Sensor,
        History
    }

    public int index;

    [SerializeField] LightController lightController;
    [SerializeField] BatteryController batteryController;
    [SerializeField] CurtainsConrtoller[] curtainConrtollers;
    [SerializeField] ConditionerController conditionerController;

    [SerializeField] TemperatureSensor temperatureSensor;

    [SerializeField] LightMode currentLightMode;
    [SerializeField] TemperatureMode currentTemperatureMode;

    int checkedHour;

    public float CurrentBatteryTemperature { get; private set; }
    public float CurretntConditionerPower { get; private set; }
    public float CurrentLightIntensity { get; private set; }

    public bool IsConditionerOn { get; private set; }
    public bool IsBatteryOn { get; private set; }


    private void Awake()
    {
        lightController = GetComponentInChildren<LightController>();
        batteryController = GetComponentInChildren<BatteryController>();
        curtainConrtollers = GetComponentsInChildren<CurtainsConrtoller>();
        conditionerController = GetComponentInChildren<ConditionerController>();

        temperatureSensor = GetComponentInChildren<TemperatureSensor>();
    }

    private void Start()
    {
    }

    private void Update()
    {
        if (currentLightMode == LightMode.Sensor)
        {
            SetLightIntensity(SettingsData.inst.sensorToLightIntCurve.Evaluate(LightSensor.currentEnvLightIntensity));
        }
        else if (currentLightMode == LightMode.History)
        {
            if (checkedHour != (int) EnvironmentController.currentHour)
            {
                checkedHour = (int) EnvironmentController.currentHour;
                float avgIntensity = 0f;

                for (int i = 0; i < 3; i++)
                {
                    var chd = SettingsData.inst.historyData.dayDatas[i].hourDatas[checkedHour];
                    var crd = chd.roomDatas[index];
                    avgIntensity += crd.lightIntensity;
                }

                SetLightIntensity(avgIntensity / 3f);
            }
        }

        if (currentTemperatureMode == TemperatureMode.History)
        {
            if (checkedHour != (int)EnvironmentController.currentHour)
            {
                checkedHour = (int)EnvironmentController.currentHour;
                float avgBatteryTemperature = 0f;
                float avgConditionerValue = 0f;

                for (int i = 0; i < 3; i++)
                {
                    var chd = SettingsData.inst.historyData.dayDatas[i].hourDatas[checkedHour];
                    var crd = chd.roomDatas[index];
                    avgBatteryTemperature += crd.batteryTemperature;
                    avgConditionerValue += crd.conditionerValue;
                }

                SetBatteryTemperature(avgBatteryTemperature/3f);
                SetConditionerPower(avgConditionerValue / 3f);
            }
        }
    }

    public void SetLightMode(LightMode lightMode)
    {
        currentLightMode = lightMode;
    }

    public void SetTemperatureMode(TemperatureMode temperatureMode)
    {
        currentTemperatureMode = temperatureMode;
    }

    public void SetLightIntensity(float intensity)
    {
        //currentLightMode = LightMode.Manual;
        CurrentLightIntensity = intensity;
        lightController.SetLightIntensity(intensity);

    }

    public void SwitchBatteryState(bool state)
    {
        batteryController.SwitchBatteryState(state);
    }

    public void SwitchConditionerState(bool state)
    {
        conditionerController.SwitchConditionerState(state);
        IsConditionerOn = state;
    }

    public void SetBatteryTemperature(float temperature)
    {
        currentTemperatureMode = TemperatureMode.Manual;
        CurrentBatteryTemperature = temperature;
        batteryController?.SetBatteryTemperature(temperature);
    }

    public void SetConditionerPower(float power)
    {
        currentTemperatureMode = TemperatureMode.Manual;
        CurretntConditionerPower = power;
        conditionerController?.SetConditionerPower(power);
    }

    public void SwitchCurtainsState(bool isOpened)
    {
        for (int i = 0; i < curtainConrtollers.Length; i++)
            curtainConrtollers[i].SwitchCurtainsState(isOpened);
    }
}
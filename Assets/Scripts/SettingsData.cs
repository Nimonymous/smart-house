﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class SettingsData : MonoBehaviour 
{
    public static SettingsData inst;

    public HistoryData historyData;

    public AnimationCurve dayTimeToHourCurve;
    public AnimationCurve sensorToLightIntCurve;
    public AnimationCurve sensorToTemperatureCurve;

    public float temperatureChangeRate;

    private void Awake()
    {
        inst = this;
    }

    private void Start()
    {
        //GenerateHistory();
        ReadHistory();
    }

    void ReadHistory()
    {
        historyData = JsonUtility.FromJson<HistoryData>(File.ReadAllText(Application.streamingAssetsPath + "/History.txt"));
    }

    void GenerateHistory()
    {
        historyData = new HistoryData();

        for (int i = 0; i < historyData.dayDatas.Length; i++)
        {
            historyData.dayDatas[i] = new DayData();
            for (int j = 0; j < historyData.dayDatas[i].hourDatas.Length; j++)
            {
                historyData.dayDatas[i].hourDatas[j] = new HourData();
                for (int k = 0; k < historyData.dayDatas[i].hourDatas[j].roomDatas.Length; k++)
                {
                    historyData.dayDatas[i].hourDatas[j].roomDatas[k] = new RoomData();
                    historyData.dayDatas[i].hourDatas[j].roomDatas[k].roomIndex = k;
                    historyData.dayDatas[i].hourDatas[j].roomDatas[k].lightIntensity = Random.Range(0f, 1.2f);
                    historyData.dayDatas[i].hourDatas[j].roomDatas[k].conditionerValue = Random.Range(0f, 36f);
                    historyData.dayDatas[i].hourDatas[j].roomDatas[k].batteryTemperature = Random.Range(0f, 5f);
                }
            }
        }

        string json = JsonUtility.ToJson(historyData);
        File.WriteAllText(Application.streamingAssetsPath + "/History.txt", json);
    }
}

[System.Serializable]
public class HistoryData
{
    public DayData[] dayDatas = new DayData[3];
}

[System.Serializable]
public class DayData
{
    public HourData[] hourDatas = new HourData[24];
}

[System.Serializable]
public class HourData
{
    public RoomData[] roomDatas = new RoomData[9];
}

[System.Serializable]
public class RoomData
{
    public int roomIndex;
    public float batteryTemperature;
    public float conditionerValue;
    public float lightIntensity;
}

[System.Serializable]
public class PresetData
{
    public int[] values;
}

[System.Serializable]
public class MethodParam
{
    public PresetData PresetData;

    public int roomIndex;

    public float lightIntensity;
    public float batteryValue;
    public float conditionerPower;

    public bool curtainsState;

    public Room.LightMode lightMode;
    public Room.TemperatureMode temperatureMode;
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TemperatureSensor : MonoBehaviour
{
    public float currentTemperature;

    [SerializeField] TextMesh roomTemperatureTxt;
    [SerializeField] Room room;

    WaitForSeconds waitForSeconds;

    string suffix;

    private void Awake()
    {
        suffix = "°";
        roomTemperatureTxt = GetComponentInChildren<TextMesh>();
        waitForSeconds = new WaitForSeconds(0.2f);
        room = GetComponentInParent<Room>();
    }

    private void Start()
    {
        StartCoroutine(GetRoomTemperature());
    }

    IEnumerator GetRoomTemperature()
    {
        while (true)
        {
            currentTemperature = CalculateTemperature();
            roomTemperatureTxt.text = string.Concat(currentTemperature.ToString("0.0"), suffix);
            yield return waitForSeconds;
        }
    }

    float CalculateTemperature()
    {
        int amount = 0;

        if (room.IsBatteryOn)
            amount++;
        if (room.IsConditionerOn)
            amount++;

        amount = 2;

        if (amount > 0)
            return (room.CurrentBatteryTemperature + room.CurretntConditionerPower) / amount + 0.1f * (EnvironmentController.currentEnvTemperature - 10f);
        else
            return (room.CurrentBatteryTemperature + room.CurretntConditionerPower) + 0.1f * (EnvironmentController.currentEnvTemperature - 10f);
    }
}

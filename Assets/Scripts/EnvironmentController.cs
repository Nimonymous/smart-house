﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnvironmentController : MonoBehaviour 
{
    public static float currentHour;
    public static float currentEnvTemperature;

    [SerializeField] AnimationCurve ambientLightCurve;
    [SerializeField] AnimationCurve dirLightAngleCurve;
    [SerializeField] Text currentEnvTemperatureTxt;
    [SerializeField] Text currentHourTxt;
    [SerializeField] GameObject roofObj;
    [SerializeField] Transform dirLight;

    float currentIntensity;

    void Start()
    {
        currentIntensity = 1f;
    }

    void Update()
    {
        RenderSettings.ambientIntensity = Mathf.Lerp(RenderSettings.ambientIntensity, currentIntensity, 5f * Time.deltaTime);
    }

    public void SetLightAngle(Slider slider)
    {
        if (slider.value <= 30f || slider.value >= 150f)
            dirLight.gameObject.SetActive(false);
        else if (!dirLight.gameObject.activeInHierarchy)
            dirLight.gameObject.SetActive(true);

        currentHour = SettingsData.inst.dayTimeToHourCurve.Evaluate(slider.value / 10);

        currentIntensity = ambientLightCurve.Evaluate(currentHour);
        

        Vector3 currentEulers = dirLight.eulerAngles;
        currentEulers.x = dirLightAngleCurve.Evaluate(currentHour);
        currentEulers.y = -30f;
        currentEulers.z = 0f;
        dirLight.eulerAngles = currentEulers;

        currentHourTxt.text = currentHour.ToString("0");
    }

    public void SetEnvironmentTemperature(Slider slider)
    {
        currentEnvTemperature = slider.value;
        currentEnvTemperatureTxt.text = slider.value.ToString();
    }

    public void SwitchRoofState(Toggle toggle)
    {
        roofObj.SetActive(toggle.isOn);
    }
}

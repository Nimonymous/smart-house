﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatteryController : MonoBehaviour 
{
    public float currentTemperature;
    float dumper;

    [SerializeField] TextMesh temperatureValueTxt;

    string suffix;

    public bool IsOn { get; private set; }

    private void Awake()
    {
        suffix = "°";
        temperatureValueTxt = GetComponentInChildren<TextMesh>();
    }

    private void Update()
    {
        dumper = Mathf.Lerp(dumper, currentTemperature, 1f * Time.deltaTime);
        temperatureValueTxt.text = string.Concat(dumper.ToString("0"), suffix);
    }

    public void SwitchBatteryState(bool state)
    {
        IsOn = state;
        temperatureValueTxt.color = IsOn ? Color.red : Color.gray;

    }

    public void SetBatteryTemperature(float temperature)
    {
        currentTemperature = temperature;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using System.Security.Cryptography;
using System.Reflection;


public class MainManager : MonoBehaviour
{
    [SerializeField] Room[] rooms;

    static Dictionary<string, Action<MethodParam>> methods;

    void Awake()
    {
        rooms = FindObjectsOfType<Room>();
        rooms = rooms.OrderBy(x => x.name).ToArray();

        methods = new Dictionary<string, Action<MethodParam>>();

        MethodInfo[] methodInfos = this.GetType().GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);

        foreach (var item in methodInfos)
            methods.Add(item.Name.ToLower(), (Action<MethodParam>) Delegate.CreateDelegate(typeof(Action<MethodParam>), this, item));
    }


    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        CallMethod("setroomlight", new MethodParam() { roomIndex = 0, lightIntensity = 1.2f });
    }

    public static void CallMethod(string methodName, MethodParam methodParam)
    {
        methods[methodName](methodParam);
    }

    public void EnableRoomBattery(MethodParam methodParam)
    {
        rooms[methodParam.roomIndex].SetTemperatureMode(Room.TemperatureMode.Manual);
        rooms[methodParam.roomIndex].SwitchBatteryState(true);
    }

    public void DisableRoomBattery(MethodParam methodParam)
    {
        rooms[methodParam.roomIndex].SetTemperatureMode(Room.TemperatureMode.Manual);
        rooms[methodParam.roomIndex].SwitchBatteryState(false);
    }

    public void EnableRoomConditioner(MethodParam methodParam)
    {
        rooms[methodParam.roomIndex].SetTemperatureMode(Room.TemperatureMode.Manual);
        rooms[methodParam.roomIndex].SwitchConditionerState(true);
    }

    public void DisableRoomConditioner(MethodParam methodParam)
    {
        rooms[methodParam.roomIndex].SetTemperatureMode(Room.TemperatureMode.Manual);
        rooms[methodParam.roomIndex].SwitchConditionerState(false);
    }

    public void SetLightMode(MethodParam methodParam)
    {
        rooms[methodParam.roomIndex].SetLightMode(methodParam.lightMode);
    }

    public void SetTemperatureMode(MethodParam methodParam)
    {
        rooms[methodParam.roomIndex].SetTemperatureMode(methodParam.temperatureMode);
    }

    public void SetLightModeAll(MethodParam methodParam)
    {
        for (int i = 0; i < rooms.Length; i++)
            rooms[i].SetLightMode(methodParam.lightMode);
    }

    public void SetTemperatureModeAll(MethodParam methodParam)
    {
        for (int i = 0; i < rooms.Length; i++)
            rooms[i].SetTemperatureMode(methodParam.temperatureMode);
    }

    public void ApplyLightPreset(MethodParam methodParam)
    {
        for (int i = 0; i < rooms.Length; i++)
            rooms[i].SetLightIntensity(methodParam.PresetData.values[i]);
    }

    public void ApplyBatteryPreset(MethodParam methodParam)
    {
        for (int i = 0; i < rooms.Length; i++)
        {
            rooms[i].SetTemperatureMode(Room.TemperatureMode.Manual);
            rooms[i].SetBatteryTemperature(methodParam.PresetData.values[i]);
        }
    }

    public void ApplyConditionerPreset(MethodParam methodParam)
    {
        for (int i = 0; i < rooms.Length; i++)
        {
            rooms[i].SetTemperatureMode(Room.TemperatureMode.Manual);
            rooms[i].SetConditionerPower(methodParam.PresetData.values[i]);
        }
    }

    public void ApplyTemperaturePreset(MethodParam methodParam)
    {

    }

    public void SetRoomLight(MethodParam methodParam)
    {
        rooms[methodParam.roomIndex].SetLightMode(Room.LightMode.Manual);
        rooms[methodParam.roomIndex].SetLightIntensity(methodParam.lightIntensity);
    }

    public void SetRoomBattery(MethodParam methodParam)
    {
        rooms[methodParam.roomIndex].SetTemperatureMode(Room.TemperatureMode.Manual);
        rooms[methodParam.roomIndex].SetBatteryTemperature(methodParam.batteryValue);
    }

    public void SetRoomConditioner(MethodParam methodParam)
    {
        rooms[methodParam.roomIndex].SetTemperatureMode(Room.TemperatureMode.Manual);
        rooms[methodParam.roomIndex].SetConditionerPower(methodParam.conditionerPower);
    }

    public void SwitchRoomCurtiansState(MethodParam methodParam)
    {
        rooms[methodParam.roomIndex].SwitchCurtainsState(methodParam.curtainsState);
    }

    public void OpenAllCurtains(MethodParam methodParam)
    {
        for (int i = 0; i < rooms.Length; i++)
            rooms[i].SwitchCurtainsState(true);
    }

    public void CloseAllCurtains(MethodParam methodParam)
    {
        for (int i = 0; i < rooms.Length; i++)
            rooms[i].SwitchCurtainsState(false);
    }
}

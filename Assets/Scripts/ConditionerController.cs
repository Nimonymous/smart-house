﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConditionerController : MonoBehaviour 
{
    public float currentPower;
    float dumper;

    [SerializeField] TextMesh conditionerPowerTxt;

    string prefix;

    public bool IsOn { get; private set; }

	void Awake () 
	{
        prefix = "C";
        conditionerPowerTxt = GetComponentInChildren<TextMesh>();
	}

    private void Update()
    {
        dumper = Mathf.Lerp(dumper, currentPower, 12f * Time.deltaTime);
        conditionerPowerTxt.text = string.Concat(prefix, dumper.ToString("0"));
    }

    public void SwitchConditionerState(bool state)
    {
        IsOn = state;
        conditionerPowerTxt.color = IsOn ? Color.red : Color.gray;
    }

    public void SetConditionerPower(float power)
    {
        currentPower = power;
    }
}
